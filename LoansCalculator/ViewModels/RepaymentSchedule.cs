﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoansCalculator.ViewModels
{
    public class Repayment
    {
        public decimal InstallmentNumber { get; set; }
        public decimal AmountDue { get; set; }
        public decimal Principal { get; set; }
        public decimal Interest { get; set; }
    }

    public class RepaymentSchedule
    {
        public IEnumerable<Repayment> Installments { get; set; }
    }
}