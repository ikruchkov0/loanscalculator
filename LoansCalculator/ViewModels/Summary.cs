﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoansCalculator.ViewModels
{
    public class Summary
    {
        public decimal WeeklyRepayment { get; set; }

        public decimal TotalRepaid { get; set; }

        public decimal TotalInterest { get; set; }
    }
}