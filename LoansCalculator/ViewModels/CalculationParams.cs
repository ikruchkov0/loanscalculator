﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace LoansCalculator.ViewModels
{
    public class CalculationParams : IValidatableObject
    {
        private const string AmountParamName = "amount";
        private const string RateParamName = "apr";
        private const string NumberOfInstallmentsParamName = "numberOfInstallments";

        [BindRequired]
        [FromQuery(Name = AmountParamName)]
        public decimal Amount { get; set; }

        [BindRequired]
        [FromQuery(Name = RateParamName)]
        public decimal Rate { get; set; }

        [FromQuery(Name = NumberOfInstallmentsParamName)]
        [Range(1, int.MaxValue)]
        public int NumberOfInstallments { get; set; }

        public CalculationParams()
        {
            NumberOfInstallments = 52;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Rate <= 0)
            {
                yield return new ValidationResult($"{RateParamName} should be greater than 0", new[] { RateParamName });
            }

            if (Amount <= 0)
            {
                yield return new ValidationResult($"{AmountParamName} should be greater than 0", new[] { AmountParamName });
            }
        }
    }
}