﻿using AutoMapper;

namespace LoansCalculator.ViewModels
{
    public static class Mappings
    {
        public static IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Math.Models.Summary, Summary>();
                cfg.CreateMap<Math.Models.Repayment, Repayment>();
            });

            return config.CreateMapper();
        }
    }
}