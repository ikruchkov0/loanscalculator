﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using AutoMapper;
using LoansCalculator.Math;
using LoansCalculator.ViewModels;

namespace LoansCalculator.Controllers
{
    [ApiController]
    public class LoansController : ControllerBase
    {
        private IMapper _mapper;

        private ILoanSummaryCalculator _summaryCalculator;

        private IInstallmentCalculator _installmentCalculator;

        private IRepaymentCalculator _repaymentCalculator;

        public LoansController(IMapper mapper, IInstallmentCalculator installmentCalculator, ILoanSummaryCalculator summaryCalculator, IRepaymentCalculator repaymentCalculator)
        {
            _mapper = mapper;
            _installmentCalculator = installmentCalculator;
            _summaryCalculator = summaryCalculator;
            _repaymentCalculator = repaymentCalculator;
        }

        [Route("loansummary")]
        [HttpGet]
        public ActionResult<Summary> Summary([BindRequired, FromQuery]CalculationParams param)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var installment = _installmentCalculator.Calculate(param.Amount, param.Rate, param.NumberOfInstallments);
            var summary = _summaryCalculator.Calculate(installment);

            return _mapper.Map<Summary>(summary);
        }

        [Route("repaymentSchedule")]
        [HttpGet]
        public ActionResult<RepaymentSchedule> RepaymentSchedule([BindRequired, FromQuery]CalculationParams param)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var installment = _installmentCalculator.Calculate(param.Amount, param.Rate, param.NumberOfInstallments);
            var repayments = _repaymentCalculator.Calculate(installment);

            return new RepaymentSchedule
            {
                Installments = repayments.Select(x => _mapper.Map<Repayment>(x)),
            };
        }
    }
}
