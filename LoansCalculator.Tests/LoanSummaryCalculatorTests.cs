using FluentAssertions;
using LoansCalculator.Math.Implementation;
using LoansCalculator.Math.Models;
using Xunit;

namespace LoansCalculator.Tests
{
    public class LoanSummaryCalculatorTests
    {
        [Fact]
        public void TestSummaryCalculation()
        {
            var installment = new Installment
            {
                Amount = 1058,
                Principal = 50000,
                NumberOfInstallments = 52,
            };

            var calculator = new LoanSummaryCalculator();

            var summary = calculator.Calculate(installment);

            summary.WeeklyRepayment.Should().Be(1058);
            summary.TotalRepaid.Should().Be(55016);
            summary.TotalInterest.Should().Be(5016);
        }
    }
}