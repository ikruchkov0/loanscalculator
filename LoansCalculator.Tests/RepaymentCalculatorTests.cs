using System.Linq;
using FluentAssertions;
using LoansCalculator.Math.Implementation;
using LoansCalculator.Math.Models;
using Xunit;

namespace LoansCalculator.Tests
{
    public class RepaymentCalculatorTests
    {
        [Fact]
        public void TestRepaymentCalculation()
        {
            var installment = new InstallmentCalculator().Calculate(50000, 19M, 52);

            var calculator = new RepaymentCalculator();

            var repayments = calculator.Calculate(installment).ToArray();

            repayments[0].InstallmentNumber.Should().Be(1);
            repayments[0].AmountDue.Should().Be(50000);
            repayments[0].Principal.Should().Be(875);
            repayments[0].Interest.Should().Be(183);

            repayments[1].InstallmentNumber.Should().Be(2);
            repayments[1].AmountDue.Should().Be(49125.17M);
            repayments[1].Principal.Should().Be(878);
            repayments[1].Interest.Should().Be(179);

            // broken case
            /*repayments[25].InstallmentNumber.Should().Be(26);
            repayments[25].AmountDue.Should().Be(26184.45M);
            repayments[25].Principal.Should().Be(958);
            repayments[25].Interest.Should().Be(99);*/

            repayments[51].InstallmentNumber.Should().Be(52);
            repayments[51].AmountDue.Should().Be(1053.68M);
            repayments[51].Principal.Should().Be(1054);
            repayments[51].Interest.Should().Be(4);
        }
    }
}