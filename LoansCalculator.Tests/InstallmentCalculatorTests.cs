using FluentAssertions;
using LoansCalculator.Math.Implementation;
using LoansCalculator.Math.Models;
using Xunit;

namespace LoansCalculator.Tests
{
    public class InstallmentCalculatorTests
    {
        [Fact]
        public void TestInstallmentCalculation()
        {
            var numberOfInstallments = 52;
            var amount = 50000M;
            var apr = 19M;

            var calculator = new InstallmentCalculator();

            var installment = calculator.Calculate(amount, apr, numberOfInstallments);

            var roundedInstallment = System.Math.Round(installment.Amount, System.MidpointRounding.ToEven);

            roundedInstallment.Should().Be(1058);

            installment.NumberOfInstallments.Should().Be(numberOfInstallments);
            installment.Principal.Should().Be(amount);
            installment.Rate.Percent.Should().Be(apr);
        }

        [Fact]
        public void TestValidation()
        {
            var numberOfInstallments = 52;
            var amount = 50000M;
            var apr = .19M;

            var calculator = new InstallmentCalculator();

            var ex = Assert.Throws<ParameterValidationException>(() => calculator.Calculate(amount, apr, -10));
            ex.Param.Should().Be(ParameterValidationException.ParamType.NumberOfInstallments);

            ex = Assert.Throws<ParameterValidationException>(() => calculator.Calculate(amount, -10, numberOfInstallments));
            ex.Param.Should().Be(ParameterValidationException.ParamType.Rate);

            ex = Assert.Throws<ParameterValidationException>(() => calculator.Calculate(-10, apr, numberOfInstallments));
            ex.Param.Should().Be(ParameterValidationException.ParamType.Amount);
        }
    }
}