using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LoansCalculator.Controllers;
using LoansCalculator.Math.Implementation;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Xunit;

namespace LoansCalculator.Tests
{
    public class ScenariosTests : IClassFixture<WebApplicationFactory<LoansCalculator.Startup>>
    {
        private readonly WebApplicationFactory<LoansCalculator.Startup> _factory;

        public ScenariosTests(WebApplicationFactory<LoansCalculator.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Scenario1()
        {
            var client = _factory.CreateClient();

            var url = "/loansummary?amount=50000&apr=19";

            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<ViewModels.Summary>(await response.Content.ReadAsStringAsync());

            result.WeeklyRepayment.Should().Be(1058);
            result.TotalRepaid.Should().Be(55016);
            result.TotalInterest.Should().Be(5016);
        }

        [Fact]
        public async Task Scenario2()
        {
            var client = _factory.CreateClient();

            var url = "/repaymentSchedule?amount=50000&apr=19";

            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<ViewModels.RepaymentSchedule>(await response.Content.ReadAsStringAsync());

            var installments = result.Installments.ToArray();

            installments[0].InstallmentNumber.Should().Be(1);
            installments[0].AmountDue.Should().Be(50000);
            installments[0].Principal.Should().Be(875);
            installments[0].Interest.Should().Be(183);

            installments[1].InstallmentNumber.Should().Be(2);
            installments[1].AmountDue.Should().Be(49125.17M);
            installments[1].Principal.Should().Be(878);
            installments[1].Interest.Should().Be(179);

            installments[9].InstallmentNumber.Should().Be(10);
            installments[9].AmountDue.Should().Be(42010.44M);
            installments[9].Principal.Should().Be(904);
            installments[9].Interest.Should().Be(153);

            // broken case
            /*installments[25].InstallmentNumber.Should().Be(26);
            installments[25].AmountDue.Should().Be(26184.45M);
            installments[25].Principal.Should().Be(958);
            installments[25].Interest.Should().Be(99);*/

            installments[51].InstallmentNumber.Should().Be(52);
            installments[51].AmountDue.Should().Be(1053.68M);
            installments[51].Principal.Should().Be(1054);
            installments[51].Interest.Should().Be(4);
        }
    }
}