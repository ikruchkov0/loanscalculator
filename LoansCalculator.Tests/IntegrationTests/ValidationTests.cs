using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Xunit;

namespace LoansCalculator.Tests
{
    public class ValidationTests : IClassFixture<WebApplicationFactory<LoansCalculator.Startup>>
    {
        public class ValidationResponse
        {
            public string[] Apr { get; set; }

            public string[] Amount { get; set; }
        }

        private readonly WebApplicationFactory<LoansCalculator.Startup> _factory;

        public ValidationTests(WebApplicationFactory<LoansCalculator.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task TestAprRequiredValidation()
        {
            var client = _factory.CreateClient();

            var url = $"/loansummary?amount={5000}";

            var response = await client.GetAsync(url);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);
            var result = JsonConvert.DeserializeObject<ValidationResponse>(await response.Content.ReadAsStringAsync());
            result.Apr.Should().NotBeNull();
            result.Apr.Length.Should().Be(1);
        }

        [Fact]
        public async Task TestAmountRequiredValidation()
        {
            var client = _factory.CreateClient();

            var url = $"/loansummary?apr={5000}";

            var response = await client.GetAsync(url);

            response.StatusCode.Should().Be(System.Net.HttpStatusCode.BadRequest);
            var result = JsonConvert.DeserializeObject<ValidationResponse>(await response.Content.ReadAsStringAsync());
            result.Amount.Should().NotBeNull();
            result.Amount.Length.Should().Be(1);
        }
    }
}