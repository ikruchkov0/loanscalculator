using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LoansCalculator.Controllers;
using LoansCalculator.Math.Implementation;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace LoansCalculator.Tests
{
    public class LoansControllerTests
    {
        [Fact]
        public void TestSummary()
        {

            var mapper = ViewModels.Mappings.Create();
            var summaryCalculator = new LoanSummaryCalculator();
            var repaymentCalculator = new RepaymentCalculator();
            var installmentCalculator = new InstallmentCalculator();

            var controller = new LoansController(mapper, installmentCalculator, summaryCalculator, repaymentCalculator);

            var param = new ViewModels.CalculationParams
            {
                Amount = 50000M,
                Rate = 19M,
            };

            var summary = controller.Summary(param).Value;

            summary.WeeklyRepayment.Should().Be(1058);
            summary.TotalRepaid.Should().Be(55016);
            summary.TotalInterest.Should().Be(5016);
        }

        [Fact]
        public void TestRepaymentSchedule()
        {
            var mapper = ViewModels.Mappings.Create();

            var installmentCalculator = new InstallmentCalculator();
            var summaryCalculator = new LoanSummaryCalculator();
            var repaymentCalculator = new RepaymentCalculator();

            var controller = new LoansController(mapper, installmentCalculator, summaryCalculator, repaymentCalculator);

            var param = new ViewModels.CalculationParams
            {
                Amount = 50000M,
                Rate = 19M,
                NumberOfInstallments = 52,
            };

            var schedule = controller.RepaymentSchedule(param).Value;

            var installments = schedule.Installments.ToArray();

            installments.Length.Should().Be(52);

            installments[0].InstallmentNumber.Should().Be(1);
            installments[0].AmountDue.Should().Be(50000);
            installments[0].Principal.Should().Be(875);
            installments[0].Interest.Should().Be(183);
        }
    }
}