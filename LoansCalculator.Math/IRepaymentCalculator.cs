﻿using System.Collections.Generic;
using LoansCalculator.Math.Models;

namespace LoansCalculator.Math
{
    public interface IRepaymentCalculator
    {
        IEnumerable<Repayment> Calculate(Installment installment);
    }
}