﻿using System;

namespace LoansCalculator.Math.Models
{
    public class ParameterValidationException : Exception
    {
        public enum ParamType
        {
            Amount,
            Rate,
            NumberOfInstallments,
        }

        public ParameterValidationException(ParamType param, string message) : base(message)
        {
            Param = param;
        }

        public ParamType Param { get; private set; }
    }
}
