﻿namespace LoansCalculator.Math.Models
{
    public struct AnnualInterestRate
    {
        public AnnualInterestRate(decimal percent)
        {
            Percent = percent;
        }

        public decimal Percent { get; private set; }

        public decimal Rate => Percent / 100M;

        public decimal CalculateInstallmentPeriodInterestRate(int numberOfInstallments)
        {
            return Rate / numberOfInstallments;
        }

        public static implicit operator AnnualInterestRate(decimal percent) => new AnnualInterestRate(percent);

        public static implicit operator decimal(AnnualInterestRate rate) => rate.Percent;
    }
}
