﻿namespace LoansCalculator.Math.Models
{
    public class Installment
    {
        public decimal Amount { get; set; }
        public int NumberOfInstallments { get; set; }
        public decimal Principal { get; set; }
        public AnnualInterestRate Rate { get; set; }

        public decimal InstallmentPeriodInterestRate => Rate.CalculateInstallmentPeriodInterestRate(NumberOfInstallments);
    }
}