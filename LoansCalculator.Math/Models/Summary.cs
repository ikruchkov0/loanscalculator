﻿namespace LoansCalculator.Math.Models
{
    public class Summary
    {
        public decimal TotalInterest { get; set; }
        public decimal TotalRepaid { get; set; }
        public decimal WeeklyRepayment { get; set; }
    }
}