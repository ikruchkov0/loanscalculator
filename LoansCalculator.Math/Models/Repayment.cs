﻿namespace LoansCalculator.Math.Models
{
    public class Repayment
    {
        public decimal AmountDue { get; set; }
        public decimal InstallmentNumber { get; set; }
        public decimal Interest { get; set; }
        public decimal Principal { get; set; }
    }
}