﻿using System.Collections.Generic;
using LoansCalculator.Math.Models;

namespace LoansCalculator.Math.Implementation
{
    public class RepaymentCalculator : IRepaymentCalculator
    {
        public IEnumerable<Repayment> Calculate(Installment installment)
        {
            var amountDue = installment.Principal;

            for (var i = 1; i <= installment.NumberOfInstallments; i++)
            {
                var interest = CalculateInterest(installment, amountDue);
                var principal = installment.Amount - interest;

                yield return new Repayment
                {
                    InstallmentNumber = i,
                    AmountDue = MoneyMath.Round(amountDue),
                    Interest = MoneyMath.RoundToInteger(interest),
                    Principal = MoneyMath.RoundToInteger(principal),
                };

                amountDue -= principal;
            }
        }

        private decimal CalculateInterest(Installment installment, decimal amountDue)
        {
            return installment.InstallmentPeriodInterestRate * amountDue;
        }
    }
}
