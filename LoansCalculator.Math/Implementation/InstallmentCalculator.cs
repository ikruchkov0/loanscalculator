﻿using LoansCalculator.Math.Models;

namespace LoansCalculator.Math.Implementation
{
    public class InstallmentCalculator : IInstallmentCalculator
    {
        public Installment Calculate(decimal amount, AnnualInterestRate rate, int numberOfInstallments)
        {
            if (amount < 0)
            {
                throw new ParameterValidationException(ParameterValidationException.ParamType.Amount, "Amount should be greater than 0");
            }

            if (rate.Percent <= 0)
            {
                throw new ParameterValidationException(ParameterValidationException.ParamType.Rate, "Rate should be greater than 0");
            }

            if (numberOfInstallments < 0)
            {
                throw new ParameterValidationException(ParameterValidationException.ParamType.NumberOfInstallments, "Number of installments should be greater than 0");
            }

            var installment = CalculateInstallment(amount, rate, numberOfInstallments);

            return new Models.Installment
            {
                Amount = installment,
                NumberOfInstallments = numberOfInstallments,
                Principal = amount,
                Rate = rate,
            };
        }

        private decimal CalculateInstallment(decimal amount, AnnualInterestRate rate, int numberOfInstallments)
        {
            var r = rate.CalculateInstallmentPeriodInterestRate(numberOfInstallments);
            return amount * r / (1 - MoneyMath.Pow(1 + r, -numberOfInstallments));
        }
    }
}