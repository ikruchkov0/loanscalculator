﻿using LoansCalculator.Math.Models;

namespace LoansCalculator.Math.Implementation
{
    public class LoanSummaryCalculator : ILoanSummaryCalculator
    {
        public Summary Calculate(Installment installment)
        {
            var weeklyRepayment = MoneyMath.RoundToInteger(installment.Amount);
            var totalRepaid = weeklyRepayment * installment.NumberOfInstallments;
            var totalInterest = totalRepaid - installment.Principal;

            return new Summary
            {
                WeeklyRepayment = weeklyRepayment,
                TotalInterest = totalInterest,
                TotalRepaid = totalRepaid,
            };
        }
    }
}
