﻿using System;

namespace LoansCalculator.Math.Implementation
{
    internal static class MoneyMath
    {
        public static decimal RoundToInteger(decimal money)
        {
            return System.Math.Round(money, MidpointRounding.ToEven);
        }

        public static decimal Round(decimal money)
        {
            return System.Math.Round(money, 2);
        }

        public static decimal Pow(decimal money, int power)
        {
            return System.Convert.ToDecimal(System.Math.Pow((double)money, power));
        }
    }
}
