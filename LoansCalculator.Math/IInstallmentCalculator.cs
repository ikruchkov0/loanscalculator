﻿using LoansCalculator.Math.Models;

namespace LoansCalculator.Math
{
    public interface IInstallmentCalculator
    {
        Installment Calculate(decimal amount, AnnualInterestRate rate, int numberOfInstallments);
    }
}