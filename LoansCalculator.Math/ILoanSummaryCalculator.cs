﻿using LoansCalculator.Math.Models;

namespace LoansCalculator.Math
{
    public interface ILoanSummaryCalculator
    {
        Summary Calculate(Installment installment);
    }
}